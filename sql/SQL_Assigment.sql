USE classicmodels;

#1 Prepare a list of offices sorted by country, state, city.
SELECT * FROM classicmodels.offices order by country,state,city;

#2 How many employees are there?
SELECT COUNT(employeeNumber) as 'Total number of employees' from classicmodels.employees;

#3 What is the total of payments received?
SELECT COUNT(checkNumber) as 'Total number of payments' from payments;

#4 List the product lines that contain 'Cars'.
SELECT productLine from productlines where productLine LIKE '%Cars%';

#5 Report total payments for October 28, 2004.
select * from payments where paymentDate LIKE ("%2004-09-28%");

#6 Report those paymentes greater than $100000. 
select * from payments where amount > 100000;

#7 List the products in each product line.
SELECT DISTINCT(productLine) from productlines;

SELECT DISTINCT(productcode),productLine from products;

#8 How many products in each product line?
SELECT count(distinct productLine) from productlines;

OR

SELECT productline,count(distinct productCode) from products group by productline;

#9 What is the minimum payment received?
SELECT min(amount) from payments;

#10 List all payments greater than twice the average payment. 
SELECT amount from payments WHERE amount > 2 * (SELECT AVG(amount) FROM payments);

#11 What is the avarage percentage markup of the MSRP on buyPrice?
Select avg(MSRP) from products;

#12 How many distinct products does ClassicModels sell? 
SELECT count(distinct productCode) from products;

#13 Report the name and city of customers who don't have sales representatives 
SELECT customername,city,salesRepEmployeeNumber FROM classicmodels.customers where salesRepEmployeeNumber is null;

#14 What are the names of executives with VP or Manager in their title? Use the CONCAT function to combine the employee's first name and last name into a single field
select concat(firstName,' ',lastName) as firstname from employees where jobTitle LIKE '%VP%' OR jobTitle LIKE '%Manager%';

#15 Which orders have a value greater than $5.000? 
SELECT orders.orderNumber,sum(orderdetails.quantityOrdered * orderdetails.priceEach) as Total FROM orders  left join classicmodels.orderdetails on orders.orderNumber = orderdetails.orderNumber GROUP BY orderNumber having Total > 5000; 

# Make a top list of customers who spend the most.
SELECT customerNumber,sum(orderdetails.quantityOrdered * orderdetails.priceEach) as Total FROM orders  left join classicmodels.orderdetails on orders.orderNumber = orderdetails.orderNumber GROUP BY orders.customerNumber order by Total desc limit 10

# Create index
create index customers_index on customers(customerNumber);
select * from customers where customerNumber = 187

