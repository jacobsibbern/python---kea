# Import libraries
import os
import csv
import sys
import time
import platform
import mysql.connector 
from mysql.connector import Error
import random

# Define global variables.
global MYSQL_HOST
global MYSQL_DATABASE
global MYSQL_USER
global MYSQL_PASSWORD
global MYSQL_PORT

# Set default values to MySQL connection variables.
MYSQL_HOST = "localhost"
MYSQL_DATABASE = "testdb"
MYSQL_USER = "test"
MYSQL_PASSWORD = "test"
MYSQL_PORT = "3306"

# Apply the global dictionary for all settings.
global config
config = {
"mysql": {
    "host": MYSQL_HOST,
    "database": MYSQL_DATABASE,
    "user": MYSQL_USER,
    "password": MYSQL_PASSWORD,
    "port": MYSQL_PORT
    }
}

# Define the main menu function.
def MAIN():
    MENU()

# Clean function to clear the terminal determined by OS.
def CLEAN():
    time.sleep(2)
    cur_os = platform.system()
    if cur_os == "Windows":
        os.system('cls')  # For Windows
    elif cur_os == "Linux" or "Darwin" :
        os.system('clear')  # For Linux/OS X
    else: 
        print("Operating system not supported for clear screen command.")

# System exit function in case I want to add multiple OS.
def SYS_EXIT():
    sys.exit

# Main menu
def MENU():
    print()
    print(" ========== MySQL Game Stats Python App ========== ")
    print()
    choice = input("""
    This games requires a MySQL database to run.
    If you haven't modified the MySQL settings 
    you should do it now. (Press 4)

    1. Create a game.
    2. Change scores for a game.
    3. Delete game. 
    4. Configure MySQL Connection Settings
    5. Show all games. 
    6. Flush ALL games.

    Please enter your choice (1-5 or 0 to exit): """)
    # Choice if/else statement and execute functions accordingly. 
    if choice == "1":
        CREATE_GAME()
    elif choice == "2":
        MANAGE_GAME()
    elif choice=="3":
        DEL_GAME()
    elif choice=="4":
        CHANGE_SETTINGS()  
    elif choice=="5":
        SHOW_GAMES()
    elif choice=="6":
        FLUSH_GAMES()
    elif choice=="0":
        SYS_EXIT()
    else:
        print("You must only enter a number from 1-5 or 0 to exit.")
        print("Please try again")
        MENU()

# Function to change MySQL settings in terminal.
def CHANGE_SETTINGS():
    MYSQL_HOST = input("Enter MySQL Host: ")
    MYSQL_DATABASE = input("Enter MySQL Database:")
    MYSQL_USER = input("Enter MySQL Username: ")
    MYSQL_PASSWORD = input("Enter MySQL Password: ")
    MYSQL_PORT = input("Enter MySQL Port: ")
    print(config["mysql"])
# Simulate game function.
def SIM_GAME():
    score = 0
    global score_total
    score_total = 0
    max_score = 10
    conn = mysql.connector.connect(host=config["mysql"]["host"],database=config["mysql"]["database"],user=config["mysql"]["user"],password=config["mysql"]["password"],port=config["mysql"]["port"])
    if conn.is_connected():
        db_Info = conn.get_server_info()
        print("Connected to MySQL server version ", db_Info)
        cursor = conn.cursor()
        while score_total < max_score:
            teamname =  "team" + str(random.randrange(1,teams)) # input(f"Name of team #{k}: ")
            cursor.execute(f"UPDATE {gamename} SET score = score + 1 where team = '{teamname}'")
            conn.commit()
            cursor.execute(f"SELECT team,score FROM {gamename}")
            result = cursor.fetchall()
            conn.commit()
            cursor.execute(f"SELECT MAX(score) FROM {gamename}")
            score_total = (cursor.fetchone())
            score_total = (score_total[0])
        cursor.execute(f"UPDATE {gamename} SET totalscore = totalscore + score")
        conn.commit()
        cursor.execute(f"update {gamename} SET totalgames = totalgames + 1")
        conn.commit()
        cursor.execute(f"update {gamename} set winrate = wins/totalgames*100")
        conn.commit()
        cursor.execute(f"SELECT team FROM {gamename} where score = \'{max_score}\'")
        score_total = (cursor.fetchone())
        conn.commit()
        score_total = (score_total[0])
        print("Game simulated and following team won the game:",score_total,"with a score of:",max_score)
        cursor.execute(f"UPDATE {gamename} SET wins = wins + 1 where team = \'{score_total}\'")
        conn.commit()
        cursor.execute(f"UPDATE {gamename} SET losses = losses + 1 WHERE NOT team = \'{score_total}\'")
        conn.commit()
        cursor.execute(f"UPDATE {gamename} set score = \'0\'")
        conn.commit()
# Create game function.
def CREATE_GAME():
    # Declare variables for creating game.
    global gamename

    gamename = input("Enter the name of the game you wish to create: ")
    global teams
    teams = int(input("How many teams do you wish to add to this game?: "))
    a = 1
    teamlist = []
    while a <= teams:
        i = 1
        for i in range(teams):
            teamname =  "team" + str(a) # input(f"Name of team #{k}: ")
            teamlist.append(teamname)
            a = a + 1
    result = ','.join(item for item in teamlist)
    print(f"Teams addded to list {result}")
    try:
        conn = mysql.connector.connect(host=config["mysql"]["host"],database=config["mysql"]["database"],user=config["mysql"]["user"],password=config["mysql"]["password"],port=config["mysql"]["port"])
        if conn.is_connected():
            db_Info = conn.get_server_info()
            print("Connected to MySQL server version ", db_Info)
            cursor = conn.cursor()
            cursor.execute(f"CREATE TABLE IF NOT EXISTS {gamename} (team varchar(20) NOT NULL PRIMARY KEY, score int(20) NOT NULL DEFAULT 0, totalscore int(20) DEFAULT 0, wins int(20) NOT NULL DEFAULT 0,losses int(20) NOT NULL DEFAULT 0, totalgames int(20) DEFAULT 0, winrate int(3) DEFAULT 0)")
            print(f"Table {gamename}_teams created.")
            teamlist_formatted_sql = ','.join('("' + item + '")' for item in teamlist)
            cursor.execute(f"INSERT INTO {gamename} (team) VALUES {teamlist_formatted_sql}")
            conn.commit()
            print(cursor.rowcount, f"Record(s) inserted successfully into {gamename} table")
            SIM_GAME()

            max_number_of_games = input("How many games should be played?: ")
            number_of_games = 1
            while number_of_games < int(max_number_of_games):
                SIM_GAME()
                number_of_games = number_of_games + 1
            cursor.execute(f"""SELECT *,wins+losses AS totalgames from {gamename}""")
            cursor.fetchall()
            conn.commit()
            cursor.execute(f"SELECT *,wins/totalgames AS winrate from {gamename}")
            cursor.fetchall()
            conn.commit()
            cursor.execute(f"SELECT * FROM {gamename}")
            result = cursor.fetchall()
            print(result)
            CLEAN()
            return MAIN()
    except Error as e:
        print("Error while executing the script -", e)
        time.sleep(5)
        return MAIN()

# Changes game stats function.
def MANAGE_GAME():
    conn = mysql.connector.connect(host=config["mysql"]["host"],database=config["mysql"]["database"],user=config["mysql"]["user"],password=config["mysql"]["password"],port=config["mysql"]["port"])
    if conn.is_connected():
        db_Info = conn.get_server_info()
        print("Connected to MySQL server version ", db_Info)
        cursor = conn.cursor()
        cursor.execute("SHOW TABLES")
        result = cursor.fetchall()
        for row in result:
            print(row[0])
        print()
    # Define gamename (table.
    gamename = input("Choose what game do you wish to manage: ")
    # Show teams
    cursor.execute(f"select team from {gamename} ")
    result = cursor.fetchall()
    filtered_list = []
    x = str.contains("team*")
    for x in result:
        filtered_list.append(x)
    print(filtered_list)
    
    team_choice = input("What team do you want to change points for?: ")

    print(f"You've choosen to manage: {gamename}")
    time.sleep(1)

    try:
        conn = mysql.connector.connect(host=config["mysql"]["host"],database=config["mysql"]["database"],user=config["mysql"]["user"],password=config["mysql"]["password"],port=config["mysql"]["port"])
        if conn.is_connected():
            db_Info = conn.get_server_info()
            print("Connected to MySQL server version ", db_Info)
            cursor = conn.cursor()


            #cursor.execute(f"INSERT INTO {gamename} ({team1},{team2}) VALUES({team1v},{team2v}")
            #result = cursor.fetchall()
            #print(result)
            #cursor.execute(f"INSERT INTO {gamename} (") 
            print()
            
            CLEAN()
            return MAIN()
    except Error as e:
        print("Error while executing the script -", e)
        CLEAN()
        return MAIN()

# Show all games function.
def SHOW_GAMES():
    try:
        conn = mysql.connector.connect(host=config["mysql"]["host"],database=config["mysql"]["database"],user=config["mysql"]["user"],password=config["mysql"]["password"],port=config["mysql"]["port"])
        if conn.is_connected():
            db_Info = conn.get_server_info()
            print("Connected to MySQL server version ", db_Info)
            cursor = conn.cursor()
            cursor.execute("SHOW TABLES")
            result = cursor.fetchall()
            for row in result:
                print(row[0])
            print()
            print("All games showed successfully.")
            CLEAN()
            return MAIN()
    except Error as e:
        print("Error while executing the script -", e)
        CLEAN()
        return MAIN()

def FLUSH_GAMES():
    try:
        conn = mysql.connector.connect(host=config["mysql"]["host"],database=config["mysql"]["database"],user=config["mysql"]["user"],password=config["mysql"]["password"],port=config["mysql"]["port"])
        if conn.is_connected():
            db_Info = conn.get_server_info()
            print("Connected to MySQL server version ", db_Info)
            cursor = conn.cursor()
            cursor.execute("SHOW TABLES")
            result = cursor.fetchall()
            for row in result:
                cursor.execute(f"DROP TABLE {row[0]}")
                print(f"Deleted game: {row[0]}")
            CLEAN()
            return MAIN()
    except Error as e:
        print("Error while executing the script -", e)
        CLEAN()
        return MAIN()

#the program is initiated, so to speak, here
MAIN()
