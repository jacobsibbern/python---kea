# Define dictionary keys & values
new_dict = {
	"Tak": 'Gracias',
	"Hej": 'Holla',
	"Mange": 'Mucho',
	"Så lidt": 'De nada'
}


# Import modules
import sys, time

# Add function for menu to be called as main()
def main():
   menu()

# Add function for menu
def menu():
	
    print("************Welcome to the Danish to English translator**************")
    print()

    # Show options
    choice = input("""
    Choose a value: 

    1: Add new key & value
    2: Update key & value
    3: Delete key & value
    4: Check if key exists
    5: Show values
    99: Exit

    Please enter your choice: """) 
    # Make if menu to choose values entered
    if choice == "1":
        menu_1()
    elif choice == "2":
        menu_2()
    elif choice == "3":
    	menu_3()
    elif choice == "4":
    	menu_4()
    elif choice == "5":
    	menu_5()
    elif choice=="99":
        sys.exit
    else: # Make sure to only enter valid options
        print("You must only select either 1 or 99")
        print("Please try again")
        menu()
# Functions for options in menu
def menu_1():
	add_key()
def menu_2():
	add_key()
def menu_3():
	del_key()
def menu_4():
	check_key()
def menu_5():
	show_values()
	time.sleep(3)
	return main()

# Menu item 4 - Define function for look up value
def checkKey(new_dict, key): 
      
    if key in dict(new_dict).keys(): # Display loop
        print("Present, ", end =" ")  # Show output if true
        print("value =", new_dict[key])  # Print new key
        return main() # Return to main menu
    else: 
        print("Not present") # If not present print message
        return check_key() # return to start of function
  
# Add function for driver code for look up value
def check_key():
	lookupkey = input("What key do you want to check?: ") # Variable (input) to look check
	checkKey(new_dict, lookupkey) 

# Show value
def show_values():
	print() # Add spacing
	print("Dansk - Espanol") # Add Title
	for k, v in new_dict.items(): # Create a loop  
		print(str(k) + " = " + str(v)) # Display the loop

def add_key(): # Define function to add key
	add_new_key = input("What key would you like to add?: ") # Define variable key
	add_new_value = input("What value would you like to add?: ") # Define variable value
	new_dict.update( {add_new_key : add_new_value} ) # Update dictionary.
	show_values() # Display values
	print() # Spacing
	print("The key",add_new_key,"with the value",add_new_value,"has been added") # Print result
	print() # Spacing
	main() # Call main menu

def del_key(): # Define function delete key
	key_to_remove = input("What key would you like to delete?: ") # Input variable key to delete
	if key_to_remove in new_dict: # If variable is in dictionary delete
		del new_dict[key_to_remove]
		show_values()
		print()
		print(key_to_remove + " has been deleted")
		main()
	else: # Else show the values in dict and try again.
		show_values()
		print()
		print(key_to_remove + " doesn't exist in dictionary")
		del_key()
#the program is initiated, so to speak, here

main()

