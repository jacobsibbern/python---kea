
# Example of Dictionary/Hash

bag = dict()
bag['money'] = 12
bag['coin'] = 5
bag['tissues'] = 7
bag['candy'] = 9
bag['trash'] = 25

print(bag)

bag['money'] = 5

print (bag)

#for s in bag:
#	print(s)


# Show keys and values in dictionary
print()
print("Display using loop")
for k, v in bag.items():
		print(str(k) + " = " + str(v))




