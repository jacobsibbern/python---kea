import os, time, sys

os.system( "cls" )

list_of_names = {}

class name_person():
	def __init__(self, name, address):
		self.name = name
		self.address = address

	def insert_person(self):
		# Print what we just added to our list
		print(self.name + " lives at " + self.address + ".")

		# Insert our newest name and address to our list (list_of_names)
		list_of_names[self.name] = self.address
		start_menu()

def print_list():
	print("Name - Address")
	print()

	# Check the length of our list and return empty if it's 0 (no items in list)
	list_len = len(list_of_names)

	if list_len > 0:
		for k, v in list_of_names.items():
			print( str(k) + " - " + str(v) )
	else:
		print("Your list is empty.")

	time.sleep(3)
	start_menu()

def create_character():
	# Input function to add our person with the inputs entered.
	name = input("Name: ")
	address = input("Address: ")

	add_person = name_person(name, address)
	add_person.insert_person()

def start_menu():
    # Show the clients options when you launch the program.
    user_choice = input("""
    What do you want to do?

    1. Add Person
    2. Print List Of Persons

    99. Exit

    Please enter your choice: """)

    # If statements to check values entered in the user_choice.
    if user_choice == "1":
        create_character()
    elif user_choice == "2":
        print_list()
    elif user_choice=="99":
        sys.exit
    else: #Make sure to only enter valid options
        print("Your only options are 1, 2 or 99.")
        print("Please try again!")
        time.sleep(1)
        start_menu()

start_menu()