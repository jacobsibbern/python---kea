
#Author : Muniba 
# Exercise 5 

age = int(input("How old are you? "))
myAge = 32

avgAge = (age+myAge)/2
print("The average of our ages is " + str(avgAge))

diff = myAge-age
# Age cannot be negative so to avoid negative differences
if age > myAge:
    diff = age-myAge
else:
    diff = myAge-age

print("The difference between our age is " + str(diff))

