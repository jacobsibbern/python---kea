# Author: Muniba  
#I am a comment 
# Only programmers can read comments 
# System ignores comments and moves to next line of code  


#store a message in a variable 
message = "Hello World"
#print a message 
print(message)

#store a new message in a same variable 
message = "Hi there :)"
#print the new message 
print(message)


#Ask the user for a message and store it in same variable 
message = input("Enter your message here: ")
#print users message 
print(message)

#print the message with three different cases from slide 67 Day-1
print(message.upper())
print(message.lower())
print(message.title())


"""
Note :- You can even comment multiple lines of codes by using 
3xdouble quotes. 
"""
