#Thegame.py by Lenka Otap, KEA, 2018
counter = 0;
choice = 0;

print("=====THE CHALLENGE GAME====")
print("Choose a number between 1-8")
print("-Choose 9 to exit the game-")
print("===========================")

choice = int(input())

while choice!=9:

	if (choice > 0 and choice <=8):
		print("You chose " + str(choice) + ". It's an ", end='')

		if choice%2==0:
			print("even number. For an even number you choose a class mate to your right.")
		else:
			print("odd number. For an odd number, you choose a class mate to your left.")

	if choice==1:
		print("Explain to this person what a variable is.")
	elif choice==2:
		print("Explain to this person what an if...else statement is.")
	elif choice==3:
		print("Explain to this person what a while loop is.")
	elif choice==4:
		print("Explain to this person what you know about datatypes.")
	elif choice==5:
		print("Explain to this person what a for loop is")
	elif choice==6:
		print("Explain to this person you are studying at KEA.")
	elif choice==7:
		print("Explain to this person which datatypes you know.")
	elif choice==8:
		print("Explain to this person the difference between = and ==.")
	else:
		print("Please choose a number between 1 and 8.")

	choice = int(input())
	counter = counter+1;

print("Thank you for playing the game.")
