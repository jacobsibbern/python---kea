#Pivot Exercise 


import csv
import matplotlib.pyplot as plt
import numpy as np

#-------- CREATE A DICTIONARY WITH UNIQUE SALES PERSON VALUES ex7.4 -----
f = open('pivot.csv', 'r')
csvreader= csv.reader(f, delimiter=';')
next(csvreader) #skip the header row

personsList = []
for row in csvreader:
    personsList.append(row[9])

#print(personsList)
personsDict = dict((key,0) for key in personsList)
print(personsDict)
f.close()

#--------- PRINT TOTAL REVENUES FOR ALL UNIQUE SALES PERSONS ex 7.5
f = open('pivot.csv', 'r')
csvreader= csv.reader(f, delimiter=';')
next(csvreader) #skip the header row

revenueSum = 0
for row in csvreader:
    row[24] = row[24][1:]
    row[24] = row[24].replace(',', '.')
    row[24] = float(row[24].replace(".", "", row[24].count(".") -1))

    for name, rev in personsDict.items():
        if row[9] == name:
            personsDict[name] += float(row[24])

for name, rev in personsDict.items():
    print(name + "s total revenue is " + str(personsDict[name]))

# ----------- PLOT NAMES vs VALUES ex 7.6 -----

plt.plot(personsDict.keys(),personsDict.values(), 'ro')
plt.title('Revenues')
plt.xlabel('Names')
plt.ylabel('Revenues')
plt.xticks(rotation=45)
plt.show() # show the plot

f.close()


#personsSet = set(personsList)
#personsList = list(personsSet)
#print(personsList)
