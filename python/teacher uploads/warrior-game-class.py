import os

os.system('cls')  # Clear cmd for Windows

class Warrior():
	def __init__(self, name, clan, strength, dexterity):
		self.name = name
		self.clan = clan
		self.strength = strength
		self.dexterity = dexterity

	def about(self):
		print(self.name + " is a warrior from the " + self.clan + " clan")
		print("Strength is " + str(self.strength))
		print("Dexterity is " + str(self.dexterity))

player1 = Warrior("Jerry", "Jerk", 23, 24)
player2 = Warrior("Peter Piper", "Douchebag", 17, 25)

player1.about()
player2.about()