#Class Exercise4
prompt = (" > ")

class CC():
    def __init__(self, card_name, card_number, exp_month, exp_year, code):
        self.card_name = card_name
        self.card_number = card_number
        self.exp_month = exp_month
        self.exp_year = exp_year
        self.code = code

    def about(self):
        print(str(self.card_name) + " with number " + str(self.card_number) + " expiring on month " + str(self.exp_month) + " of " + str(self.exp_year))

global card1
card1 = CC("Mastercard", 12345678901, 12, 18, 145)
global card2
card2 = CC("Visa", 918765432109, 7, 11, 123)

def fruitbuy():
    print("Which fruit would you like to buy? We have the following: ")
    database = {"apple" : 20, "banana" : 25, "kiwi" : 45, "carrot" : 30}

    for key,value in database.items():
        print(str(key) + "\t = \t" + str(value))
    choice_fruit = str(input(prompt))

    print("You have 2 cards on file: card1 and card2")
    print("Which card to use?")
    choice_card = str(input())

    print("You have 2 cards on file: card1 and card2")

    if choice_card == "card1":
        print("You are buying a " + str(choice_fruit) + " using your: ")
        card1.about()
    elif choice_card == "card2":
        print("You are buying a " + str(choice_fruit) + " using your: ")
        card2.about()
    else:
        print("Not a valid card.")

fruitbuy()
