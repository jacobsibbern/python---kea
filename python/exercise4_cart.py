import time

our_items = {
	"Apple": 3,
	"Banana": 4,
	"Orange": 5,
	"Watermelon": 6
}

class CC():
	def __init__( self ):
		self.mastercard = "Mastercard"
		self.visa = "Visa"

		print(self.mastercard)
		print(self.visa)

def buy_fruits():
	our_money = 100

	for items in our_items:
		print(items)

	#purchasing the fruits with an input to choose fruits
	item_to_buy = input("Which of the following fruits would you like to purchase?")

	# setting the price as a variable
	price = int(our_items[item_to_buy])
	print( "You've choosen to purchase an " + item_to_buy + ". It costs", price, "dollars to purchase." )

	#choose a card with an input for the user
	card_to_use = input("Select card to purchase with:")
	print("You've choosen to buy the " + item_to_buy + " with your " + card_to_use)

	#printing our balance after purchasing
	print("Your balance is now:")

	our_money = our_money - price
	print(our_money)

	#buy more/restart function
	time.sleep(2)
	return buy_fruits()

buy_fruits()
