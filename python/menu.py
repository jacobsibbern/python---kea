#Netflix type system demo - FakeFlix
import csv
import sys
import datetime

def main():
   menu()


def menu():
    print("************Choose a script to run**************")
    print()

    choice = input("""
                      1: Student list
                      2: test.py
                      3: thegame.py
                      4: Tell the time
                      99: Exit

                      Please enter your choice: """)

    if choice == "1":
        menu_1()
    elif choice == "2":
        menu_2()
    elif choice == "3":
    	menu_3()
    elif choice == "4":
    	menu_4()
    elif choice=="99":
        sys.exit
    else:
        print("You must only select either 1 or 99")
        print("Please try again")
        menu()

def menu_1():
   exec(open("./hi.py").read())
def menu_2():
   exec(open("./test.py").read())
def menu_3():
   exec(open("./thegame.py").read())
def menu_4():
	print("The time and date is:",datetime.datetime.utcnow())

    
#the program is initiated, so to speak, here
main()