# Remember to install matplotlib first by doing: 

# python -mpip install -U matplotlib # Windows 
# python3 -mpip install -U matplotlib # MacOS
# sudo apt-get install python3-matplotlib # Linux


import matplotlib.pyplot as plt
import numpy as np

x = np.linespace(-10,10)

y = np.sin(x)

plt.plot(x,y, 'b',label='sin')
plt.plot(x,3*y, 'r',label='2*sin')
plt.plot(x,2*y, 'g',label='3*sin')

plt.legend()
plt.title("Sine Function")
plt.xlabel("Input")
plt.ylabel("Output")
plt.show()