import matplotlib.pyplot as plt

x = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
y = [8,25,6,9,12,15,14]

plt.style.use('ggplot')
plt.plot(x,y, 'r',label='Weekdays')
plt.title('Cube numbers')
plt.xlabel('Days')
plt.legend()
plt.ylabel('Kms')
plt.show()