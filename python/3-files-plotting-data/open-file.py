# Either: fileHandle = open(r'test.txt') OR fileHandle = open('test.txt','r')
# r = read-only mode
# rb = read-only(binary)
# r+ = read + write
# w = write-only
# wb = write-only (binary)
# w+ = writng + reading
# wb+ = writing + reading (binary)
# a = append
# ab = append (binary)
# a+ = append + read
# ab+ = append + read (binary)

# Useful file functios:
# close() = close file
# detach() = returns the separated raw stream from the buffer
# fileno() = returns a number that represents the stream from the opearting system's perspective
# flush()  = fushes internal buffer
# isatty() = returns whether the file stream is interactive or not
# read() = returns the file content
# readable() = returns whether the file stream can be read or not
# readline() = returns one line from the file
# seek() = change the file position
# seekable() = returns wether the file allows us to change the file position
# tell() = returns the current file position
# truncate() = resize the file to a specified size
# writeable() = returns if writeable
# write() = write to file
# writelines() write to list of strings

import csv

fileHandle = open(r'test.txt')

def search_in_file():

	searchstr = input("What do you need to search for?")

	for line in fileHandle:
		if line.startswith(searchstr):
			print("Found: ",searchstr)

def count_lines():
	count = 0
	for line in fileHandle:
		count = count + 1
	print('Line Count:', count)

def read_print():
	fileText = fileHandle.read()
	print("Length of Text in File:",len(fileText))
	print("Text in the File: ", fileText[:])

def open_csv():
	fh = open('file.csv','r')
	csvreader = csv.reader(fh,delimiter=';')

	csvList = []
	for row in csvreader:
		csvList.append(str(row))

	for line in csvList:
		print(line)

def advanced_csv():
	fh = open('file.csv', 'r')
	csvreader = csv.reader(fh, delimiter=';')
	next(csvreader)

	fnameList = []
	lnameList = []
	addressList = []
	stateList = []
	stateinList = []
	zipcodeList = []

	for row in csvreader:
		if any(row):
			fnameList.append(str(row[0]))
			lnameList.append(str(row[1]))
			addressList.append(str(row[2]))
			stateList.append(str(row[3]))
			stateinList.append(str(row[4]))
			zipcodeList.append(str(row[5]))

	print(fnameList)
	print(lnameList)
	print(addressList)
	print(stateList)
	print(stateinList)
	print(zipcodeList)


advanced_csv()
