
-- Disse restriktioner er en god ide at brug for at det kommer ind på den rigtige måde.

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- Sørge for at vi er i den korrekte database.

USE `coderstrust_group_13` ;

-- -----------------------------------------------------
-- Pladsholder tabel for view `coderstrust_group_13`.`Mentors` 
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Mentors` (`ID` INT, `Fullname` INT, `Country` INT, `Zipcode` INT, `City` INT, `Address` INT, `'Role'` INT, `'Avarage Score'` INT);

-- -----------------------------------------------------
-- Pladsholder tabel for view `coderstrust_group_13`.`Freelancers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Freelancers` (`ID` INT, `Fullname` INT, `Country` INT, `Zipcode` INT, `City` INT, `Address` INT, `'Role'` INT, `'Avarage Score'` INT);

-- -----------------------------------------------------
-- Placeholder table for view `coderstrust_group_13`.`Customers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Customers` (`ID` INT, `Fullname` INT, `Country` INT, `Zipcode` INT, `City` INT, `Address` INT, `'Role'` INT, `'Avarage Score'` INT, `'Ordered Gigs'` INT, `'Total Paid'` INT);

-- -----------------------------------------------------
-- Pladsholder tabel for view `coderstrust_group_13`.`Students`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Students` (`ID` INT, `Fullname` INT, `Country` INT, `Zipcode` INT, `City` INT, `Address` INT, `'Role'` INT, `'Avarage Score'` INT, `'Completed Courses'` INT);

-- -----------------------------------------------------
-- Pladsholder tabel for view `coderstrust_group_13`.`Employees`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Employees` (`ID` INT, `Fullname` INT, `Country` INT, `Zipcode` INT, `City` INT, `Address` INT, `'Role'` INT, `'Revenue'` INT, `'Total Orders'` INT);

-- -----------------------------------------------------
-- Pladsholder tabel for view `coderstrust_group_13`.`Categories Overview`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Categories Overview` (`name` INT, `'Products'` INT);

-- -----------------------------------------------------
-- Pladsholder tabel for view `coderstrust_group_13`.`All Orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`All Orders` (`'Order ID'` INT, `'Product Category'` INT, `'Product name'` INT, `'Customer'` INT, `'Fullname'` INT, `'Order Status'` INT, `'Progress'` INT, `'Score'` INT, `'Reviews'` INT, `'Total Amount'` INT);

-- -----------------------------------------------------
-- Pladsholder tabel for view `coderstrust_group_13`.`Posted Gigs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Posted Gigs` (`'Gig ID'` INT, `'Gig Name'` INT, `'Price'` INT, `'Category'` INT, `'Role'` INT, `'Mentor'` INT);

-- -----------------------------------------------------
-- Pladsholder tabel for view `coderstrust_group_13`.`Available Courses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Available Courses` (`'Course ID'` INT, `'Course Name'` INT, `'Price'` INT, `'Course Duration'` INT, `'Course Category'` INT, `'Role'` INT, `'Mentor'` INT);

-- -----------------------------------------------------
-- Pladsholder tabel for view `coderstrust_group_13`.`Certificates`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Certificates` (`'Order ID'` INT, `'Product Category'` INT, `'Product name'` INT, `'Student'` INT, `'Fullname'` INT, `'Order Status'` INT, `'Progress'` INT, `'Score'` INT, `'Total Amount'` INT);

-- -----------------------------------------------------
-- Pladsholder tabel for view `coderstrust_group_13`.`Completed Gigs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Completed Gigs` (`'Order ID'` INT, `'Product Category'` INT, `'Product name'` INT, `'Customer'` INT, `'Fullname'` INT, `'Order Status'` INT, `'Progress'` INT, `'Reviews'` INT, `'Total Amount'` INT);

-- -----------------------------------------------------
-- Opret view `coderstrust_group_13`.`Mentors`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Mentors`;
DROP VIEW IF EXISTS `coderstrust_group_13`.`Mentors` ;
USE `coderstrust_group_13`;
CREATE  OR REPLACE VIEW `Mentors` AS
SELECT acc.ID,acc.Fullname,Country,Zipcode,City,Address,roles.name as 'Role',avg(orders.Score) as 'Avarage Score' FROM coderstrust_group_13.acc left join acc_roles on acc.ID = acc_roles.Acc_ID left join roles on roles.id = acc_roles.Roles_ID left join product_has_acc on acc.id = product_has_acc.acc_id left join product on product_has_acc.Product_ID = product.id left join order_has_product on product.id = order_has_product.product_id left join orders on order_has_product.Order_ID = orders.id group by acc.id having roles.name = 'Mentor';

-- -----------------------------------------------------
-- Opret view `coderstrust_group_13`.`Freelancers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Freelancers`;
DROP VIEW IF EXISTS `coderstrust_group_13`.`Freelancers` ;
USE `coderstrust_group_13`;
CREATE  OR REPLACE VIEW `Freelancers` AS
SELECT acc.ID,acc.Fullname,Country,Zipcode,City,Address,roles.name as 'Role',avg(orders.Score) as 'Avarage Score' FROM coderstrust_group_13.acc left join acc_roles on acc.ID = acc_roles.Acc_ID left join roles on roles.id = acc_roles.Roles_ID left join product_has_acc on acc.id = product_has_acc.acc_id left join product on product_has_acc.Product_ID = product.id left join order_has_product on product.id = order_has_product.product_id left join orders on order_has_product.Order_ID = orders.id group by acc.id having roles.name = 'Freelancer';

-- -----------------------------------------------------
-- Opret view `coderstrust_group_13`.`Customers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Customers`;
DROP VIEW IF EXISTS `coderstrust_group_13`.`Customers` ;
USE `coderstrust_group_13`;
CREATE  OR REPLACE VIEW `Customers` AS
SELECT acc.ID,acc.Fullname,Country,Zipcode,City,Address,roles.name as 'Role',avg(orders.Score) as 'Avarage Score',(count(order_has_customer.Acc_ID)) as 'Ordered Gigs',sum(product.price) as 'Total Paid' FROM acc left join acc_roles on acc.ID = acc_roles.Acc_ID left join roles on roles.id = acc_roles.Roles_ID left join order_has_customer on acc.id = order_has_customer.acc_ID left join orders on order_has_customer.order_id = orders.id left join order_has_product on orders.id = order_has_product.Order_ID left join product on order_has_product.Product_ID = product.id group by acc.id having roles.name = 'Customer';

-- -----------------------------------------------------
-- Opret view `coderstrust_group_13`.`Students`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Students`;
DROP VIEW IF EXISTS `coderstrust_group_13`.`Students` ;
USE `coderstrust_group_13`;
CREATE  OR REPLACE VIEW `Students` AS
SELECT acc.ID,acc.Fullname,Country,Zipcode,City,Address,roles.name as 'Role',avg(orders.Score) as 'Avarage Score',(count(order_has_customer.Acc_ID) ) as 'Completed Courses' FROM acc left join acc_roles on acc.ID = acc_roles.Acc_ID left join roles on roles.id = acc_roles.Roles_ID left join order_has_customer on acc.id = order_has_customer.acc_ID left join orders on order_has_customer.order_id = orders.id group by acc.id having roles.name = 'Student';

-- -----------------------------------------------------
-- Opret view `coderstrust_group_13`.`Employees`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Employees`;
DROP VIEW IF EXISTS `coderstrust_group_13`.`Employees` ;
USE `coderstrust_group_13`;
CREATE  OR REPLACE VIEW `Employees` AS
SELECT acc.ID,acc.Fullname,Country,Zipcode,City,Address,roles.name as 'Role',sum(product.price) as 'Revenue',count(order_has_product.order_id) as 'Total Orders' FROM acc left join acc_roles on acc.ID = acc_roles.Acc_ID left join roles on roles.id = acc_roles.Roles_ID left join product_has_acc on acc.id = product_has_acc.Acc_ID left join product on product_has_acc.product_id = product.id left join order_has_product on product.id = order_has_product.Product_ID group by acc.id having roles.name = 'Employee';

-- -----------------------------------------------------
-- Opret viewe `coderstrust_group_13`.`Categories Overview`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Categories Overview`;
DROP VIEW IF EXISTS `coderstrust_group_13`.`Categories Overview` ;
USE `coderstrust_group_13`;
CREATE  OR REPLACE VIEW `Categories Overview` AS
SELECT categories.name,count(product.id) as 'Products' from categories left join product_has_categories on categories.id = product_has_categories.categories_ID left join product on product_has_categories.Product_ID = product.id  group by categories.name;

-- -----------------------------------------------------
-- Opret view `coderstrust_group_13`.`All Orders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`All Orders`;
DROP VIEW IF EXISTS `coderstrust_group_13`.`All Orders` ;
USE `coderstrust_group_13`;
CREATE  OR REPLACE VIEW `All Orders` AS
SELECT orders.id as 'Order ID',categories.name as 'Product Category',product.name as 'Product name',acc.id as 'Customer',acc.fullname as 'Fullname',status.name as 'Order Status',progress.name as 'Progress',orders.score as 'Score',orders.reviews as 'Reviews',(product.price) as 'Total Amount' FROM orders left join order_has_product on orders.id = order_has_product.Order_ID left join product on order_has_product.Product_ID = product.id left join product_has_categories on product.id = product_has_categories.product_id left join categories on product_has_categories.categories_ID = categories.id left join order_has_customer on orders.id = order_has_customer.order_id left join acc on order_has_customer.acc_id = acc.id left join order_has_status on orders.id = order_has_status.Order_ID left join status on order_has_status.status_id = status.id left join order_has_progress on orders.id = order_has_progress.order_id left join progress on order_has_progress.progress_id = progress.id group by orders.id order by orders.id;

-- -----------------------------------------------------
-- Opret view `coderstrust_group_13`.`Posted Gigs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Posted Gigs`;
DROP VIEW IF EXISTS `coderstrust_group_13`.`Posted Gigs` ;
USE `coderstrust_group_13`;
CREATE  OR REPLACE VIEW `Posted Gigs` AS
select product.id as 'Gig ID',product.name as 'Gig Name',product.price as 'Price',categories.name as 'Category',roles.name as 'Role',acc.Fullname as 'Mentor' from product left join product_has_categories on product.id = product_has_categories.product_id left join categories on product_has_categories.categories_ID = categories.id left join product_has_acc on product.id = product_has_acc.product_id left join acc on product_has_acc.acc_id = acc.id left join acc_roles on acc.ID = acc_roles.Acc_ID left join roles on roles.id = acc_roles.Roles_ID where categories.id = '10000' order by product.id;

-- -----------------------------------------------------
-- Opret view `coderstrust_group_13`.`Available Courses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Available Courses`;
DROP VIEW IF EXISTS `coderstrust_group_13`.`Available Courses` ;
USE `coderstrust_group_13`;
CREATE  OR REPLACE VIEW `Available Courses` AS
select product.id as 'Course ID',product.name as 'Course Name',product.price as 'Price',product.duration as 'Course Duration',categories.name as 'Course Category',roles.name as 'Role',acc.Fullname as 'Mentor' from product left join product_has_categories on product.id = product_has_categories.product_id left join categories on product_has_categories.categories_ID = categories.id left join product_has_acc on product.id = product_has_acc.product_id left join acc on product_has_acc.acc_id = acc.id left join acc_roles on acc.ID = acc_roles.Acc_ID left join roles on roles.id = acc_roles.Roles_ID where categories.parentid = '20000' order by product.id;

-- -----------------------------------------------------
-- Opret view `coderstrust_group_13`.`Certificates`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Certificates`;
DROP VIEW IF EXISTS `coderstrust_group_13`.`Certificates` ;
USE `coderstrust_group_13`;
CREATE  OR REPLACE VIEW `Certificates` AS
SELECT orders.id as 'Order ID',categories.name as 'Product Category',product.name as 'Product name',acc.id as 'Student',acc.fullname as 'Fullname',status.name as 'Order Status',progress.name as 'Progress',orders.score as 'Score',(product.price) as 'Total Amount' FROM orders left join order_has_product on orders.id = order_has_product.Order_ID left join product on order_has_product.Product_ID = product.id left join product_has_categories on product.id = product_has_categories.product_id left join categories on product_has_categories.categories_ID = categories.id left join order_has_customer on orders.id = order_has_customer.order_id left join acc on order_has_customer.acc_id = acc.id left join order_has_status on orders.id = order_has_status.Order_ID left join status on order_has_status.status_id = status.id left join order_has_progress on orders.id = order_has_progress.order_id left join progress on order_has_progress.progress_id = progress.id group by orders.id having progress.name = 'Completed' and not categories.name = 'Market'order by orders.id ;

-- -----------------------------------------------------
-- Opret view `coderstrust_group_13`.`Completed Gigs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Completed Gigs`;
DROP VIEW IF EXISTS `coderstrust_group_13`.`Completed Gigs` ;
USE `coderstrust_group_13`;
CREATE  OR REPLACE VIEW `Completed Gigs` AS
SELECT orders.id as 'Order ID',categories.name as 'Product Category',product.name as 'Product name',acc.id as 'Customer',acc.fullname as 'Fullname',status.name as 'Order Status',progress.name as 'Progress',orders.Reviews as 'Reviews',(product.price) as 'Total Amount' FROM orders left join order_has_product on orders.id = order_has_product.Order_ID left join product on order_has_product.Product_ID = product.id left join product_has_categories on product.id = product_has_categories.product_id left join categories on product_has_categories.categories_ID = categories.id left join order_has_customer on orders.id = order_has_customer.order_id left join acc on order_has_customer.acc_id = acc.id left join order_has_status on orders.id = order_has_status.Order_ID left join status on order_has_status.status_id = status.id left join order_has_progress on orders.id = order_has_progress.order_id left join progress on order_has_progress.progress_id = progress.id group by orders.id having progress.name = 'Completed' and categories.name = 'Market'order by orders.id ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
