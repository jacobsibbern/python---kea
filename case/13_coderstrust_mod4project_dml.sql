-- -----------------------------------------------------
-- INDSÆT PRODUKTER I PRODUKTTABELLEN.
-- -----------------------------------------------------
START TRANSACTION;
USE `coderstrust_group_13`;
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (1, 'Intro To Java', 20, '2:30');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (2, 'WordPress Customization', 30, '4:00');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (3, 'Intro to Python', 25, '6:00');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (4, 'Intro to Freelancing', 38, '6:00');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (5, 'Intro to Digital Marketing', 34, '12:00');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (6, 'Intro to Graphic Design', 28, '4:00');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (7, 'Intro to Web Development', 32, '13:00');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (8, 'Intro to Excel & Google Sheets', 42, '1:30');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (9, 'Intro to Android App Development', 23, '4:00');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (10, 'Intro to Back-End Development', 24, '800');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (11, 'Intro to Search Engine Optimization', 25, '6:30');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (12, 'Intro to Bookkeeping & Account Management', 45, '5:40');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (13, 'Coming Soon: Intro to JavaScript', 32, '3:30');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (14, 'Coming Soon: Intro to User Testing', 34, '2:20');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (15, 'Coming Soon: Become a Virtual Assistant', 25, '1:30');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (16, 'Coming Soon: Intro to Data', 85, '7:30');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (17, 'Coming Soon: Video Editing', 94, '6:45');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (18, 'Coming Soon: Intro to Front-End Development', 55, '7:00');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (19, 'Coming Soon: Intro to Business English', 75, '4:10');
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (20, 'Optimize Website Performance', 120, NULL);
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (21, 'Optimize SEO & Organic Traffic', 160, NULL);
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (22, 'Write product description text for websites', 240, NULL);
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (23, 'Hire a Java Developer', 300, NULL);
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (24, 'Hire a Python Developer', 300, NULL);
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (25, 'Hire a Website Developer', 300, NULL);
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (26, 'Quote on project', 1000, NULL);
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (27, 'Social Media Expert', 150, NULL);
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (28, 'ERP Consultant', 200, NULL);
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (29, 'Get help with your business', 80, NULL);
INSERT INTO `coderstrust_group_13`.`Product` (`ID`, `Name`, `Price`, `Duration`) VALUES (30, 'Server Expert', 250, NULL);

COMMIT;


-- -----------------------------------------------------
-- INDSÆT PRODUKTKATEGORIER.
-- -----------------------------------------------------
START TRANSACTION;
USE `coderstrust_group_13`;
INSERT INTO `coderstrust_group_13`.`Categories` (`ID`, `Name`, `ParentID`) VALUES (10000, 'Market', NULL);
INSERT INTO `coderstrust_group_13`.`Categories` (`ID`, `Name`, `ParentID`) VALUES (20000, 'Academy', NULL);
INSERT INTO `coderstrust_group_13`.`Categories` (`ID`, `Name`, `ParentID`) VALUES (20100, 'Business', 20000);
INSERT INTO `coderstrust_group_13`.`Categories` (`ID`, `Name`, `ParentID`) VALUES (20200, 'Creative', 20000);
INSERT INTO `coderstrust_group_13`.`Categories` (`ID`, `Name`, `ParentID`) VALUES (20300, 'Data', 20000);
INSERT INTO `coderstrust_group_13`.`Categories` (`ID`, `Name`, `ParentID`) VALUES (20400, 'Development', 20000);
INSERT INTO `coderstrust_group_13`.`Categories` (`ID`, `Name`, `ParentID`) VALUES (20500, 'Digital Skills', 20000);
INSERT INTO `coderstrust_group_13`.`Categories` (`ID`, `Name`, `ParentID`) VALUES (20600, 'Marketing', 20000);
INSERT INTO `coderstrust_group_13`.`Categories` (`ID`, `Name`, `ParentID`) VALUES (20700, 'Mobile', 20000);
COMMIT;


-- -----------------------------------------------------
-- INDSÆT VÆRDIER TIL AT KNYTTE ET PRODUKT TIL FLERE KATEGORI.
-- -----------------------------------------------------
START TRANSACTION;
USE `coderstrust_group_13`;
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (1, 20400);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (2, 20400);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (3, 20400);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (4, 20500);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (5, 20600);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (6, 20200);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (7, 20400);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (8, 20300);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (9, 20700);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (10, 20400);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (11, 20600);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (12, 20100);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (13, 20400);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (14, 20100);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (15, 20300);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (16, 20200);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (17, 20400);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (18, 20500);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (19, 20600);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (20, 10000);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (21, 10000);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (22, 10000);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (23, 10000);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (24, 10000);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (25, 10000);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (26, 10000);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (27, 10000);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (28, 10000);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (29, 10000);
INSERT INTO `coderstrust_group_13`.`Product_has_Categories` (`Product_ID`, `Categories_ID`) VALUES (30, 10000);
COMMIT;

-- -----------------------------------------------------
-- INDSÆT KONTI TIL KONTOTABELLEN.
-- -----------------------------------------------------
START TRANSACTION;
USE `coderstrust_group_13`;
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (1,"Nora Swanson","Angola","1642","Rutigliano","825-9127 Nonummy Avenue");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (2,"Aubrey Michael","Sint Maarten","21488","Cherain","2712 Semper. Rd.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (3,"Callie Sellers","India","12557","Camrose","P.O. Box 699, 6439 Odio. Ave");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (4,"Eaton Blevins","Saint Lucia","01913","Kakinada","Ap #366-2211 At, Avenue");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (5,"Francis Velasquez","Finland","60384-283","Ulhasnagar","Ap #909-1462 Nonummy Ave");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (6,"Christian House","American Samoa","52985","Kyshtym","2963 Nulla Av.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (7,"Wing Perez","Korea, North","67489","Butzbach","580-3485 Ut Rd.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (8,"Steel Hubbard","Monaco","77248","Portobuffolè","486-8967 Luctus Street");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (9,"Maggie Sears","Turkmenistan","7493","Wageningen","Ap #456-9276 Fusce Avenue");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (10,"Seth Avery","Niger","4892","Tongrinne","7204 Ac Rd.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (11,"Dolan Ortega","Malawi","42937","Warburg","522-3159 Nec, St.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (12,"Leilani Hudson","Iceland","90009","Shillong","1804 Ligula Road");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (13,"Quynn Cole","Thailand","24428","Greenlaw","Ap #595-1309 Dictum St.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (14,"Denton Cook","Sudan","1112","Kawerau","717-1414 Vivamus Street");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (15,"Ramona Saunders","Sri Lanka","782575","Ville-en-Hesbaye","P.O. Box 538, 2745 Id, Ave");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (16,"Serena Michael","Saint Kitts and Nevis","27741","Eisenstadt","Ap #477-2480 Nunc Ave");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (17,"Allen Battle","Kazakhstan","22405-35751","Betim","Ap #181-2299 Lectus Road");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (18,"Christopher Pope","United Kingdom (Great Britain)","495412","Profondeville","P.O. Box 310, 6681 Interdum St.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (19,"Vincent Ratliff","Israel","78192","Springfield","207-4742 Blandit. St.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (20,"Roth Durham","Japan","43894","Merksem","P.O. Box 391, 4646 Et, Rd.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (21,"Josephine Osborne","Antigua and Barbuda","34708","Ribnitz-Damgarten","Ap #493-8501 Purus, Rd.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (22,"Clare Mason","Portugal","38085","Geel","Ap #887-374 Cras St.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (23,"Jason Combs","Albania","31416","Chambave","867-9224 Sed, Avenue");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (24,"Robin Griffin","Bermuda","694567","Zaltbommel","668-963 Natoque Rd.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (25,"Lacota Ramirez","Dominican Republic","338108","Gandhidham","P.O. Box 229, 5761 Et St.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (26,"Sierra Martinez","Singapore","5415","Pollein","P.O. Box 715, 3766 Duis Rd.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (27,"Nina Acosta","Taiwan","44-467","Torgnon","Ap #249-3769 Ac Av.");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (28,"Jana Mueller","South Sudan","670130","Trollhättan","Ap #716-6319 Aliquet Ave");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (29,"Katelyn Warren","Czech Republic","85894","Yurzhnouralsk","P.O. Box 897, 2928 Hendrerit. Road");
INSERT INTO `coderstrust_group_13`.`Acc` (`ID`,`Fullname`,`Country`,`Zipcode`,`City`,`Address`) VALUES (30,"Lacey Ortega","Seychelles","29179","Sgonico","Ap #459-8827 Mollis. St.");
COMMIT;

-- -----------------------------------------------------
-- INDSÆT ROLLE, STATUS OG FREMSKRIDT VÆRDIER.
-- -----------------------------------------------------
START TRANSACTION;
USE `coderstrust_group_13`;
INSERT INTO `coderstrust_group_13`.`Roles` (`ID`,`Name`) VALUES (1,'Mentor'),(2,'Student'),(3,'Freelancer'),(4,'Employee'),(5,'Customer');
INSERT INTO `coderstrust_group_13`.`Status` (`ID`,`Name`) VALUES (1,'Unpaid'),(2,'Paid'),(3,'Completed'),(4,'Refunded'),(5,'Cancelled');
INSERT INTO `coderstrust_group_13`.`Progress` (`ID`,`Name`) VALUES (1,'Started'),(2,'Completed');
COMMIT;

-- -----------------------------------------------------
-- INDSÆT VÆRDIER TIL AT KNYTTE EN BRUGER TIL FLERE ROLLE.
-- -----------------------------------------------------
START TRANSACTION;
USE `coderstrust_group_13`;
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (4,1);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (5,2);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (1,3);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (5,4);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (3,5);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (1,6);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (5,7);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (2,8);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (2,9);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (1,10);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (3,11);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (2,12);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (1,13);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (3,14);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (4,15);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (3,16);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (1,17);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (3,18);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (5,19);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (3,20);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (2,21);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (1,22);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (2,23);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (3,24);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (4,25);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (1,26);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (2,27);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (1,28);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (4,29);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (2,30);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (5,9);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (5,8);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (5,12);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (5,21);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (5,23);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (5,27);
INSERT INTO `coderstrust_group_13`.`Acc_roles` (`Roles_ID`,`Acc_ID`) VALUES (5,30);
COMMIT;

-- -----------------------------------------------------
-- INDSÆT ORDRE TIL ORDRETABELLEN.
-- -----------------------------------------------------
START TRANSACTION;
USE `coderstrust_group_13`;
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (1,"10A89918-1930-9FC9-8D4E-80958AD9DB21","2020-10-21 06:26:03",3,3);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (2,"33C356B2-1136-FAFB-3936-B163D72D73BC","2020-12-08 03:55:35",4,2);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (3,"C6A62D7E-6ADA-8781-CE56-279FAF58F6BE","2020-11-06 12:12:02",5,1);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (4,"E0C62552-51A8-6A3C-05D8-7A22E37BB265","2020-03-08 03:00:12",5,2);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (5,"FDFDFB5F-0E30-D717-F060-AE40E28C8B3F","2019-10-01 14:19:57",1,1);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (6,"033AA29E-AE44-E4B8-2D19-E8F8F61FB707","2021-02-15 18:27:51",5,5);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (7,"F593F323-D764-9DD7-CD0F-05F4841E53E9","2019-12-10 07:07:09",1,2);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (8,"8C9F94F9-2998-1FDD-8EC4-517FD5CDADCB","2020-06-13 00:39:30",5,1);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (9,"A441A53A-4A0D-9A04-FF37-A43280FC86EB","2020-08-28 07:43:32",2,3);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (10,"CD1A0D67-654F-463C-AA67-9C7B3FC11CB0","2020-02-17 08:07:54",4,1);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (11,"8B3C37D9-6885-53E5-5C14-A7D78EF0C11B","2020-02-04 12:14:34",5,1);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (12,"DE2F57F6-EC78-CEA5-9998-F44E29164821","2019-05-14 04:22:45",4,1);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (13,"633E588B-7A34-06E7-205F-1584684CE01F","2019-06-16 23:06:29",5,5);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (14,"207F8CE6-C1AA-570D-77D8-5EBCCCC9C17D","2020-02-22 14:10:05",1,4);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (15,"F7E3E67D-80D9-EA00-6855-4CD019F003FE","2020-08-24 14:05:21",4,3);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (16,"B62553A9-5F18-8794-B380-AA56B79ADEC3","2019-05-20 15:14:25",2,2);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (17,"C29C838A-6988-582C-5B3D-6FE29B82766D","2019-03-13 03:09:01",4,3);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (18,"9E558E62-7D32-F909-1CE0-19034714820E","2020-11-21 16:06:11",5,5);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (19,"AD704428-03A0-64EF-A019-CBB84D936116","2019-07-06 12:12:51",5,1);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (20,"DAA82ADA-6127-83B9-456E-C5F06CD427B7","2021-02-17 01:56:44",4,4);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (21,"3C3DA7CA-3735-6A73-7143-6A997D02DC87","2019-07-30 03:36:20",3,3);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (22,"A80CC50B-84CD-D7D1-B1B8-EE5F8B900229","2020-12-12 23:24:28",5,3);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (23,"7C456404-9465-0E8C-4B68-3CB984F7E4AE","2021-01-06 16:28:00",1,5);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (24,"9A64DF91-7195-A075-FAEF-C3A954E3C289","2020-12-30 21:43:31",4,3);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (25,"F9F8CF64-1B41-7097-549B-86D81231C23A","2019-03-28 17:52:52",4,2);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (26,"F0AFD0FE-2D2B-9451-FB73-FFF7D416E36D","2020-06-07 11:56:14",2,1);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (27,"3CD78879-62DC-DECF-22F2-8A19E88846E7","2021-01-31 08:58:05",1,2);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (28,"A45ADFD4-4E3C-B55D-1C41-E035BE05B587","2020-05-07 22:24:18",4,4);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (29,"41D14BA1-F8B6-A3F2-0FD4-78D9BB90A9C4","2020-01-12 20:02:53",5,4);
INSERT INTO `coderstrust_group_13`.`orders` (`ID`,`PaymentID`,`Orderdate`,`Score`,`Reviews`) VALUES (30,"CC77783E-9567-AB3F-A780-43C76AD5B841","2021-02-11 18:10:26",1,1);
COMMIT;

-- -----------------------------------------------------
-- INDSÆT VÆRDIER TIL AT KNYTTE EN ORDRE TIL EN PROGRESS ID FOR AT VIDE HVOR LANGT MAN ER I FORLØBET.
-- -----------------------------------------------------
START TRANSACTION;
USE `coderstrust_group_13`;
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (1,1);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (2,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (3,1);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (4,1);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (5,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (6,1);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (7,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (8,1);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (9,1);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (10,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (11,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (12,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (13,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (14,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (15,1);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (16,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (17,1);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (18,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (19,1);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (20,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (21,1);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (22,1);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (23,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (24,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (25,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (26,1);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (27,2);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (28,1);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (29,1);
INSERT INTO `coderstrust_group_13`.`order_has_progress` (`Order_ID`,`Progress_ID`) VALUES (30,2);
COMMIT;

-- -----------------------------------------------------
-- INDSÆT VÆRDIER TIL AT KNYTTE PRODOKTER TIL ORDRE.
-- -----------------------------------------------------
START TRANSACTION;
USE `coderstrust_group_13`;
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (1,9);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (2,19);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (3,5);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (4,16);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (5,12);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (6,1);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (7,2);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (8,18);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (9,11);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (10,14);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (11,10);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (12,17);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (13,18);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (14,17);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (15,12);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (16,7);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (17,11);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (18,6);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (19,17);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (20,20);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (21,21);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (22,22);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (23,23);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (24,24);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (25,25);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (26,26);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (27,27);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (28,28);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (29,29);
INSERT INTO `coderstrust_group_13`.`Order_Has_product` (`Order_ID`,`Product_ID`) VALUES (30,30);
COMMIT;

-- -----------------------------------------------------
-- INDSÆT VÆRDIER TIL AT SAMMENKÆDE EN KONTO TIL EN ORDRE (KUNDE).
-- -----------------------------------------------------
START TRANSACTION;
USE `coderstrust_group_13`;
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (1,8);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (2,9);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (3,12);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (4,21);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (5,23);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (6,27);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (7,30);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (8,8);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (9,9);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (10,12);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (11,21);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (12,23);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (13,27);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (14,30);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (15,8);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (16,9);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (17,12);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (18,21);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (19,23);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (20,2);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (21,4);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (22,7);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (23,19);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (24,2);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (25,4);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (26,2);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (27,4);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (28,7);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (29,7);
INSERT INTO `coderstrust_group_13`.`order_has_customer` (`Order_ID`,`Acc_ID`) VALUES (30,3);
COMMIT;

-- -----------------------------------------------------
--  INDSÆT VÆRDIER TIL AT SAMMENKÆDE MENTORE TIL KURSER.
-- -----------------------------------------------------
START TRANSACTION;
USE `coderstrust_group_13`;
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (12,3);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (14,3);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (6,6);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (16,6);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (8,10);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (15,10);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (1,13);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (2,13);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (3,13);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (7,13);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (10,10);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (13,10);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (17,10);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (4,17);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (18,17);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (5,22);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (11,26);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (19,28);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (9,10);
COMMIT;

-- -----------------------------------------------------
--  INDSÆT VÆRDIER TIL AT SAMMENKÆDE FREELANCERS TIL GIGS.
-- -----------------------------------------------------
START TRANSACTION;
USE `coderstrust_group_13`;
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (20,5);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (21,11);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (22,14);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (23,16);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (24,18);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (25,20);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (26,24);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (27,5);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (28,11);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (29,14);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (29,16);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (30,18);
COMMIT;

-- -----------------------------------------------------
-- INDSÆT VÆRDIER TIL AT SAMMENKÆDE ANSATTE TIL GIGS.
-- -----------------------------------------------------
START TRANSACTION;
USE `coderstrust_group_13`;
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (20,1);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (21,15);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (22,25);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (23,29);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (24,1);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (25,15);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (26,25);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (27,29);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (28,1);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (29,15);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (29,25);
INSERT INTO `coderstrust_group_13`.`product_has_acc` (`Product_ID`,`Acc_ID`) VALUES (30,29);
COMMIT;

-- -----------------------------------------------------
-- INDSÆT VÆRDIER TIL AT KNYTTE EN STATUS TIL EN ORDRE.
-- -----------------------------------------------------
START TRANSACTION;
USE `coderstrust_group_13`;
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (1,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (2,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (3,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (4,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (5,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (6,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (7,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (8,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (9,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (10,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (11,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (12,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (13,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (14,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (15,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (16,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (17,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (18,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (19,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (20,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (21,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (22,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (23,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (24,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (25,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (26,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (27,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (28,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (29,3);
INSERT INTO `coderstrust_group_13`.`order_has_status` (`Order_ID`,`Status_ID`) VALUES (30,3);
COMMIT;
