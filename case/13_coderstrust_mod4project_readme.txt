
# Future-Proofing the Freelancers in the Gig Platform Economy 

This script attempts to create a database structured for a market and academy application. It can be further developed to match a website requirement.

### Requirements

A MySQL Community Server version 8.*
Access to MySQL Workbench tool. (You can paste the commands via a commandline but we will only describe how to do it via workbench)

## Usage Guide

Content overview:

13_coderstrust_mod4project_ddl - Containts all the creation of tables and relationships.
13_coderstrust_mod4project_dml - All test data is inserted in this file.
13_coderstrust_mod4project_views_selects.sql - Inserts all views created to better understand the database structure. 

1. Copy the 3 files to a folder on your computer. 

2. Open MySQL workbench and connect to a MySQL server that you created.

3. Open a new SQL query by clicking the "New SQL Query" icon.

4. In this window you first paste the contents of one of the 3 files you copied before "13_coderstrust_mod4project_ddl.sql".

5. After you've successfully pasted the whole contents of the file 13_coderstrust_mod4project_ddl you have to run the query by clicking on the left yellow lightning icon. 

6. Next you have to repeat the process from step 3-5 with the other two files but remember to first run file "13_coderstrust_mod4project_dml.sql"  and afterwads "13_coderstrust_mod4project_views_selects.sql"

## Running the tests

If all rows returned a successful message then the import went correctly. If not go back and try the steps again to be sure. 

## Built With

* [MySQL DBMS](https://www.mysql.com/ https://dev.mysql.com/downloads/mysql/) - Database Management System
* [MySQL Workbench](https://dev.mysql.com/downloads/workbench/) - MySQL workbench management tool.

## Ejere

Gruppe 13 - Coderstrust assignment.
Andreas Steen Stasig
Emil Hansen
Jacob Sibbern
Magnus Hoffmann Bjørkvig
Rasmus Skettrup.