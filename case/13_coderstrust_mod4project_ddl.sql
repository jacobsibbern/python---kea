
-- Not required but helps with data integrity by limiting actions and you can safely ignore the warnings in the console.

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Slet database schema coderstrust_group_13 (navn på databasen) hvis den eksisterer
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `coderstrust_group_13` ;

-- -----------------------------------------------------
-- Opret schema coderstrust_group_13
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `coderstrust_group_13` DEFAULT CHARACTER SET utf8 ;
USE `coderstrust_group_13` ;

-- -----------------------------------------------------
-- Opret kontotabel `coderstrust_group_13`.`Acc` for alle konti
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Acc` ;

CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Acc` (
  `ID` INT NOT NULL AUTO_INCREMENT COMMENT 'Konto ID unik nøgle.',
  `Fullname` VARCHAR(150) NULL COMMENT 'Fuldenavn',
  `Country` VARCHAR(150) NULL COMMENT 'Land',
  `Zipcode` VARCHAR(20) NULL COMMENT 'Postnr.',
  `City` VARCHAR(45) NULL COMMENT 'By',
  `Address` VARCHAR(150) NULL COMMENT 'Adresse',
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Opret rolletabel `coderstrust_group_13`.`Roles`til alle roller
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Roles` ;

CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Roles` (
  `ID` INT NOT NULL AUTO_INCREMENT COMMENT 'Unik rolle ID',
  `Name` VARCHAR(150) NULL COMMENT 'Rolle navn',
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Opret hjælpetabel `coderstrust_group_13`.`Acc_Roles` for sammenkædning af roller og konti.
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Acc_Roles` ;

CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Acc_Roles` (
  `Roles_ID` INT NOT NULL COMMENT 'Foreign key til rolletabellen',
  `Acc_ID` INT NOT NULL COMMENT 'foreign key til kontotabellen.',
  PRIMARY KEY (`Roles_ID`, `Acc_ID`),
  INDEX `fk_Roles_has_Acc_Acc1_idx` (`Acc_ID` ASC) VISIBLE,
  INDEX `fk_Roles_has_Acc_Roles_idx` (`Roles_ID` ASC) VISIBLE,
  CONSTRAINT `fk_Roles_has_Acc_Roles`
    FOREIGN KEY (`Roles_ID`)
    REFERENCES `coderstrust_group_13`.`Roles` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Roles_has_Acc_Acc1`
    FOREIGN KEY (`Acc_ID`)
    REFERENCES `coderstrust_group_13`.`Acc` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Opret produkttabel. `coderstrust_group_13`.`Product` til alle produkter.
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Product` ;

CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Product` (
  `ID` INT NOT NULL AUTO_INCREMENT COMMENT 'Produkt ID unik nøgle',
  `Name` VARCHAR(150) NULL COMMENT 'Produkt navn til at beskrive produktet',
  `Price` DOUBLE NULL COMMENT 'Produkt pris, bliver brugt til at beregne total ordre beløb.',
  `Duration` TIME NULL COMMENT 'Beskrive varighed for produktet',
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Opret produktkategoritabel `coderstrust_group_13`.`Categories` til alle produktkategorier.
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Categories` ;

CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Categories` (
  `ID` INT NOT NULL AUTO_INCREMENT COMMENT 'Kategori ID Alt med 1 først er Market produkter og alt med først 2 er Academy. ',
  `Name` VARCHAR(150) NULL COMMENT 'Kategori navn',
  `ParentID` INT NULL COMMENT 'Hvis du ønsker at placerer produktgruppe  i en undergruppe så indtaster du navnet på den overgruppe du ønsker at have i parentID',
  PRIMARY KEY (`ID`),
  INDEX `fk_Categories_Categories1_idx` (`ParentID` ASC) VISIBLE,
  CONSTRAINT `fk_Categories_Categories1`
    FOREIGN KEY (`ParentID`)
    REFERENCES `coderstrust_group_13`.`Categories` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Opret hjælpetabel `coderstrust_group_13`.`Product_has_Categories` til produkter og kategorier.
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Product_has_Categories` ;

CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Product_has_Categories` (
  `Product_ID` INT NOT NULL COMMENT 'Foreign keys field for product',
  `Categories_ID` INT NOT NULL COMMENT 'Foreign keys field for product',
  PRIMARY KEY (`Product_ID`, `Categories_ID`),
  INDEX `fk_Course_has_Course_Categories_Course_Categories1_idx` (`Categories_ID` ASC) VISIBLE,
  INDEX `fk_Course_has_Course_Categories_Course1_idx` (`Product_ID` ASC) VISIBLE,
  CONSTRAINT `fk_Course_has_Course_Categories_Course1`
    FOREIGN KEY (`Product_ID`)
    REFERENCES `coderstrust_group_13`.`Product` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Course_has_Course_Categories_Course_Categories1`
    FOREIGN KEY (`Categories_ID`)
    REFERENCES `coderstrust_group_13`.`Categories` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Opret ordretabel `coderstrust_group_13`.`Orders` til alle ordre informationer.
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Orders` ;

CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Orders` (
  `ID` INT NOT NULL COMMENT 'Ordre ID som unik nøgle.',
  `PaymentID` VARCHAR(36) NULL COMMENT 'BetalingsID til at kunne finde betaling til hver ordre.',
  `Orderdate` DATETIME NULL COMMENT 'Ordredato',
  `Score` INT(1) NULL COMMENT 'Score fra 1-5',
  `Reviews` INT(1) NULL COMMENT 'Anmeldelser til freelancere og ansatte. 1-5',
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `PaymentID_UNIQUE` (`PaymentID` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Opret hjælpetabel `coderstrust_group_13`.`Order_has_Product` til at kæde produkter og ordre sammen.
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Order_has_Product` ;

CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Order_has_Product` (
  `Order_ID` INT NOT NULL COMMENT 'Foreign nøgle til ordertabellen.',
  `Product_ID` INT NOT NULL COMMENT 'Foreign nøgle til produkttabellen.',
  INDEX `fk_Order_has_Product_Product1_idx` (`Product_ID` ASC) VISIBLE,
  INDEX `fk_Order_has_Product_Order1_idx` (`Order_ID` ASC) VISIBLE,
  PRIMARY KEY (`Order_ID`, `Product_ID`),
  CONSTRAINT `fk_Order_has_Product_Order1`
    FOREIGN KEY (`Order_ID`)
    REFERENCES `coderstrust_group_13`.`Orders` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Order_has_Product_Product1`
    FOREIGN KEY (`Product_ID`)
    REFERENCES `coderstrust_group_13`.`Product` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Opret hjælpetabel `coderstrust_group_13`.`Order_has_Customer` til at knytte en kunde til en ordre.
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Order_has_Customer` ;

CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Order_has_Customer` (
  `Order_ID` INT NOT NULL COMMENT 'Foreign key til ordretabellen',
  `Acc_ID` INT NOT NULL COMMENT 'Foreign key til kontotabellen.',
  PRIMARY KEY (`Order_ID`, `Acc_ID`),
  INDEX `fk_Order_has_Acc_Acc1_idx` (`Acc_ID` ASC) VISIBLE,
  INDEX `fk_Order_has_Acc_Order1_idx` (`Order_ID` ASC) VISIBLE,
  CONSTRAINT `fk_Order_has_Acc_Order1`
    FOREIGN KEY (`Order_ID`)
    REFERENCES `coderstrust_group_13`.`Orders` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Order_has_Acc_Acc1`
    FOREIGN KEY (`Acc_ID`)
    REFERENCES `coderstrust_group_13`.`Acc` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Opret statustabel `coderstrust_group_13`.`Status` for alle status værdier som Cancelled, Refunded, Completed.
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Status` ;

CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Status` (
  `ID` INT NOT NULL COMMENT 'ID som Unik nøgle',
  `Name` VARCHAR(45) NULL COMMENT 'Status navn',
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Opret hjælpetabel `coderstrust_group_13`.`Order_has_Status` til at knytte en status til en ordre.
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Order_has_Status` ;

CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Order_has_Status` (
  `Order_ID` INT NOT NULL COMMENT 'Foreign nøgle til ordretabellen',
  `Status_ID` INT NOT NULL COMMENT 'Foreign nøgle til status tabellen',
  PRIMARY KEY (`Order_ID`, `Status_ID`),
  INDEX `fk_Order_has_Status_Status1_idx` (`Status_ID` ASC) VISIBLE,
  INDEX `fk_Order_has_Status_Order1_idx` (`Order_ID` ASC) VISIBLE,
  CONSTRAINT `fk_Order_has_Status_Order1`
    FOREIGN KEY (`Order_ID`)
    REFERENCES `coderstrust_group_13`.`Orders` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Order_has_Status_Status1`
    FOREIGN KEY (`Status_ID`)
    REFERENCES `coderstrust_group_13`.`Status` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Opret hjælpetabel `coderstrust_group_13`.`Product_has_Acc` for at knytte freelancer og mentor til produkter ud fra hvilken rolle de er.
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Product_has_Acc` ;

CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Product_has_Acc` (
  `Product_ID` INT NOT NULL COMMENT 'Foreign key til produkttabellen.',
  `Acc_ID` INT NOT NULL COMMENT 'Foreign key til kontotabellen',
  PRIMARY KEY (`Product_ID`, `Acc_ID`),
  INDEX `fk_Product_has_Acc_Acc2_idx` (`Acc_ID` ASC) VISIBLE,
  INDEX `fk_Product_has_Acc_Product2_idx` (`Product_ID` ASC) VISIBLE,
  CONSTRAINT `fk_Product_has_Acc_Product2`
    FOREIGN KEY (`Product_ID`)
    REFERENCES `coderstrust_group_13`.`Product` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Product_has_Acc_Acc2`
    FOREIGN KEY (`Acc_ID`)
    REFERENCES `coderstrust_group_13`.`Acc` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Opret progresstabel `coderstrust_group_13`.`Progress` til at se kursets eller giggets færdig status.
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Progress` ;

CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Progress` (
  `ID` INT NOT NULL COMMENT 'Fremdrift ID unik nøgle',
  `Name` VARCHAR(45) NULL COMMENT 'Fremdrift status navn',
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Opret hjælpetabel  `coderstrust_group_13`.`Order_has_progress` for at knytte en progress værdi til en ordre.
-- -----------------------------------------------------
DROP TABLE IF EXISTS `coderstrust_group_13`.`Order_has_progress` ;

CREATE TABLE IF NOT EXISTS `coderstrust_group_13`.`Order_has_progress` (
  `Order_ID` INT NOT NULL COMMENT 'Foreign key til ordretabellen',
  `Progress_ID` INT NOT NULL COMMENT 'Foriegn key til fremdriftstabellen.',
  PRIMARY KEY (`Order_ID`, `Progress_ID`),
  INDEX `fk_Progress_has_Orders_Orders1_idx` (`Order_ID` ASC) VISIBLE,
  INDEX `fk_Progress_has_Orders_Progress1_idx` (`Progress_ID` ASC) VISIBLE,
  CONSTRAINT `fk_Progress_has_Orders_Progress1`
    FOREIGN KEY (`Progress_ID`)
    REFERENCES `coderstrust_group_13`.`Progress` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Progress_has_Orders_Orders1`
    FOREIGN KEY (`Order_ID`)
    REFERENCES `coderstrust_group_13`.`Orders` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;